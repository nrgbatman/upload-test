﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Librari___TeamWork.Core.Contracts
{
    public interface IReader
    {
        string ReadLine();
    }
}

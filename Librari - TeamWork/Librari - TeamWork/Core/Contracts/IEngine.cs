﻿

using Librari___TeamWork.Core.Contracts;
using Library.Contracts;
using System.Collections.Generic;

namespace Librari___TeamWork.Contracts
{
    public interface IEngine
    {
        void Start();
        IReader Reader { get; set; }
        IWriter Writer { get; set; }
        IParser Parser { get; set; }

      
    }
}

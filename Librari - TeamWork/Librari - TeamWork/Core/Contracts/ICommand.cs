﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Librari___TeamWork.Core.Contracts
{
    public interface ICommand
    {
        string Execute(IList<string> parameters);

    }
}

﻿using System;
using Librari___TeamWork.Core.Providers;
using System.Collections.Generic;
using System.Text;

namespace Librari___TeamWork.Core.Messages
{
    public static class Messages
    {
   
        public static void WelcomeToLibrary()
        {
            Console.WriteLine($"========================================" + "\n" +
                              $"Welcome to José Vasconcelos Mexico City Library Management System" + "\n" +
                              $"Bienvenidos al Sistema de Gestión de Bibliotecas José Vasconcelos" + "\n");
        }
        public static void EnterUsername()
        {
            Console.Write("Please enter your username: ");
        }
        public static void EnterPassword()
        {
            Console.Write("Please enter your password: ");
        }
        public static void ReEnterPassword()
        {
            Console.Write("Please re-enter your password:");
        }
        public static void WaitingForCommand()
        {
            Console.Write("System is waiting for your command: ");

        }
        public static void EnterNewPassword()
        {
            Console.Write("Please enter new password: ");
        }
        public static void ReEnterNewPassword()
        {
            Console.Write("Please re-enter new password: ");
        }
        public static string InvalidCommand()
        {
            //Check this message
            return "Invalid command! Use help for list of user commands!";
        }
        public static string InvalidCredentials()
        {
            return "Invalid username or password";
        }

    }
}

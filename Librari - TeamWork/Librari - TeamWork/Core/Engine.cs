﻿using Librari___TeamWork.Contracts;
using Librari___TeamWork.Database;
using Librari___TeamWork.Users.Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using Librari___TeamWork.Core.Messages;
using Librari___TeamWork.Core.Providers;
using Librari___TeamWork.Core.Contracts;
using Librari___TeamWork.Users;

namespace Librari___TeamWork
{
    public class Engine : IEngine
    {
        private static IEngine instanceHolder;
        private const string TerminationCommand = "logout";
        private const string NullProvidersExceptionMessage = "cannot be null.";
        public Engine()
        {
            this.Reader = new ConsoleReader();
            this.Writer = new ConsoleWriter();
            this.Parser = new CommandParser();
           

        }

        public static IEngine Instance
        {
            get
            {
                if (instanceHolder == null)
                {
                    instanceHolder = new Engine();
                }

                return instanceHolder;
            }
        }
        public IReader Reader { get; set; }

        public IWriter Writer { get; set; }

        public IParser Parser { get; set; }

        public void Start()
        {
            

            while (true)
            {

                var login = new Login();
                login.LogIn();


                string commandAsString = null;
                
                while (commandAsString != "logout")
                {
                    Messages.WaitingForCommand();
                    try
                    {
                        commandAsString = this.Reader.ReadLine();

                        if (commandAsString.ToLower() == TerminationCommand.ToLower())
                        {
                            break;
                        }

                        this.ProcessCommand(commandAsString);
                    }
                    catch (Exception ex)
                    {
                        this.Writer.WriteLine(ex.Message);
                    }
                }
            }

        }

        private void ProcessCommand(string commandAsString)
        {
            if (string.IsNullOrWhiteSpace(commandAsString))
            {
                throw new ArgumentNullException("Command cannot be null or empty.");
            }

            var command = this.Parser.ParseCommand(commandAsString);
            var parameters = this.Parser.ParseParameters(commandAsString);

            var executionResult = command.Execute(parameters);
            this.Writer.WriteLine(executionResult);
        }
        public void LoadData()
        {
            var loadData = new LoadingData();
            loadData.LoadingUserData();
            loadData.LoadingBooksData();
        }
    }
}

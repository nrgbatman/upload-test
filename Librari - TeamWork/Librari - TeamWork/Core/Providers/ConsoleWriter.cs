﻿using Librari___TeamWork.Core.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace Librari___TeamWork.Core.Providers
{
    public class ConsoleWriter : IWriter
    {
        public void Write(string message)
        {
            Console.Write(message);
        }

        public void WriteLine(string message)
        {
            Console.WriteLine(message);
        }
    }
}

﻿using Librari___TeamWork.Contracts;
using Librari___TeamWork.Core.Contracts;
using Librari___TeamWork.Core.Factories;
using Librari___TeamWork.Database;
using Librari___TeamWork.Users;
using Librari___TeamWork.Users.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace Librari___TeamWork.Core.Providers
{
    public class Login : ILogin
    {
        public Login()
        {
           this.Reader = new ConsoleReader();
        }
        
        public IReader Reader { get; set; }
         
        public void LogIn()
        {
            Tuple<bool, int> validation = new Tuple<bool, int>(false, 0);

            while (!validation.Item1 == true)
            {
                //Try  to remove all thase messages!
                Messages.Messages.WelcomeToLibrary();
                Messages.Messages.EnterUsername();
                string user = Reader.ReadLine();
                Messages.Messages.EnterPassword();
                string pass = Reader.ReadLine();

                validation = DataValidation.UserValidation(user, pass);

                if (validation.Item1 == false)
                {
                    Messages.Messages.InvalidCredentials();
                }

                var loggedInAs = UserData.users[validation.Item2];

                if (validation.Item1 == true)
                {
                    Console.WriteLine(loggedInAs.ToString());
                }
            }
        }
    }
}

﻿using Librari___TeamWork.Core.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace Librari___TeamWork.Core.Providers
{
    public class ConsoleReader : IReader
    {
        public string ReadLine()
        {
            return Console.ReadLine();
        }
    }
}

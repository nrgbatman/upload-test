﻿using Librari___TeamWork.Contracts;
using Librari___TeamWork.Core.Contracts;
using Librari___TeamWork.Core.Factories;
using Librari___TeamWork.Database;
using Library;
using System;
using System.Collections.Generic;
using System.Text;

namespace Librari___TeamWork.Core.Commands
{
    public class ReserveBookCommand : ICommand
    {
        private readonly ILibraryFactory factory;
        private readonly IEngine engine;

        public ReserveBookCommand(ILibraryFactory factory, IEngine engine)
        {
            this.factory = factory;
            this.engine = engine;
        }
        public string Execute(IList<string> parameters)
        {
            int id;
            var currentUsername = UserData.users[DataValidation.userListId].UserName;
            try
            {
                id = int.Parse(parameters[0]);
            }
            catch (Exception)
            {

                throw new ArgumentException("Please enter id");
            }

            if (BooksData.bookList.Count == 0)
            {
                throw new ArgumentException("Library is empty");
            }

            foreach (var book in BooksData.bookList)
            {
                if (book.Id == id && book.BookItem.BookItemCount == 0)
                {
                    foreach (var userReserved in ReservedBooks.reservedBooks[id])
                    {
                        if (userReserved == currentUsername)
                        {
                            throw new ArgumentException("You have already reserved this book!");
                        }
                    }
                    ReservedBooks.reservedBooks[id].Add(currentUsername);
                }
            }

            return $"Book with ID: {id} has been reserved.";

            // search id
            // bookCount == 0 ---> reserve
            // who reserves
            // what is reserved
            // when
        }
    }
}

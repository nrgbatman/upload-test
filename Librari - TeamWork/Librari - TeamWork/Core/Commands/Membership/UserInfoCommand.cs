﻿using Librari___TeamWork.Contracts;
using Librari___TeamWork.Core.Contracts;
using Librari___TeamWork.Core.Factories;
using Librari___TeamWork.Database;
using Librari___TeamWork.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Librari___TeamWork.Core.Commands
{
    public class UserInfoCommand : ICommand
    {
        private readonly ILibraryFactory factory;
        private readonly IEngine engine;

        public UserInfoCommand(ILibraryFactory factory, IEngine engine)
        {
            this.factory = factory;
            this.engine = engine;
        }

        public string Execute(IList<string> parameters)
        {
            ////Checking type of logged user
            if (!DataValidation.IsLibrarianStatus())
            {
                throw new ArgumentException(Messages.Messages.InvalidCommand());
            }

            if (parameters.Count != 1)
            {
                //Think about better msg
                throw new ArgumentException("Please enter username.");
            }

            string userName;

            try
            {
                userName = parameters[0];
            }
            catch (Exception)
            {
                throw new ArgumentException("Invalid UserInfo command parameters!");
            }

            //We are checking for userName into UsersList;
            if (UserData.users.Where(x => x.UserName.ToLower() == userName.ToLower()).ToList().Count > 0)
            {
                //ADD blabla INFO firstName/lastName Membership etc
                var user = (Member)UserData.users.Where(x => x.UserName.ToLower() == userName.ToLower()).FirstOrDefault();
                StringBuilder str = new StringBuilder();
                str.AppendLine();
                str.AppendLine($"============= User {user.UserName} details =============");
                str.AppendLine($"{user.FirstName} {user.LastName} registered at {user.RegisteredAt.ToLongDateString()}");
                str.AppendLine($"Current membership status: {user.Membership}");
                str.AppendLine("=============== Books rented ===============");

                foreach (var item in UsersBooksLists.userBooksLists[userName])
                {

                    str.AppendLine($"Book - {item.Title} " +
                                   $"Author - {item.Author} " +
                                   $"Year - {item.Year} " +
                                   $"CheckOutAt - {item.BookItem.ItemCheckout.ToLongDateString()}");


                }
                return str.ToString();
            }

            //If username not found=>
            return $"User not found!";
        }
    }
}

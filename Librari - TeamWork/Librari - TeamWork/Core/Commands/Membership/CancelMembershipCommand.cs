﻿using Librari___TeamWork.Contracts;
using Librari___TeamWork.Core.Contracts;
using Librari___TeamWork.Core.Factories;
using Librari___TeamWork.Database;
using System;
using System.Collections.Generic;

namespace Librari___TeamWork.Core.Commands
{
    public class CancelMembershipCommand : ICommand
    {
        private readonly ILibraryFactory factory;
        private readonly IEngine engine;
        public CancelMembershipCommand(ILibraryFactory factory, IEngine engine)
        {
            this.factory = factory;
            this.engine = engine;
        }
        public string Execute(IList<string> parameters)
        {
            //Add validation - only members can cancel membership
            if (DataValidation.IsLibrarianStatus())
            {
                throw new ArgumentException(Messages.Messages.InvalidCommand());
            }

            if (parameters.Count != 0)
            {
                throw new ArgumentException("Cancel membership command doesn't require any parameters");
            }

            if (UserData.users[DataValidation.userListId].Membership)
            {
                Messages.Messages.EnterPassword();
                string checkingPassword = Console.ReadLine();

                if (UserData.users[DataValidation.userListId].Password == checkingPassword)
                {

                    UserData.users[DataValidation.userListId].Membership = false;
                    return $"Membership cancelled successfully";

                }

                 return "Invalid password";
            }

            return "Member does not exist";
        }
    }
}

﻿using Librari___TeamWork.Contracts;
using Librari___TeamWork.Core.Contracts;
using Librari___TeamWork.Core.Factories;
using Librari___TeamWork.Database;
using System;
using System.Collections.Generic;

namespace Librari___TeamWork.Core.Commands.Membership
{
    public class EditMemberPasswordCommand : ICommand
    {
        private readonly ILibraryFactory factory;
        private readonly IEngine engine;

        public EditMemberPasswordCommand(ILibraryFactory factory, IEngine engine)
        {
            this.factory = factory;
            this.engine = engine;
        }

        public string Execute(IList<string> parameters)
        {
            //Check status of current user
            if (!DataValidation.IsLibrarianStatus())
            {
                throw new ArgumentException(Messages.Messages.InvalidCommand());
            }

            string user;

            try
            {
                user = parameters[0];
            }
            catch (Exception)
            {

                throw new ArgumentException("Edit Member Password command invalid parameters");
            }

            foreach (var users in UserData.users)
            {
                if (users.UserName == user)
                {
                    Messages.Messages.EnterNewPassword();
                    string newPassword = Console.ReadLine();
                    Messages.Messages.ReEnterNewPassword();

                    if (newPassword == Console.ReadLine())
                    {

                        users.Password = newPassword;

                        return $"Member Password successfully changed.";
                    }
                }
            }

            return $"Invalid password";
        }
    }
}

﻿using Librari___TeamWork.Contracts;
using Librari___TeamWork.Core.Contracts;
using Librari___TeamWork.Core.Factories;
using Librari___TeamWork.Database;
using System;
using System.Collections.Generic;


namespace Librari___TeamWork.Core.Commands.Membership
{
    public class RenewMembershipCommand : ICommand
    {
        private readonly ILibraryFactory factory;
        private readonly IEngine engine;

        public RenewMembershipCommand(ILibraryFactory factory, IEngine engine)
        {
            this.factory = factory;
            this.engine = engine;
        }

        public string Execute(IList<string> parameters)
        {

            //Checking type of logged user
            if (!DataValidation.IsLibrarianStatus())
            {
                throw new ArgumentException(Messages.Messages.InvalidCommand());
            }

            string username;

            if (parameters.Count != 1)
            {
                throw new ArgumentException("Invalid parameters. Please enter username");
            }

            username = parameters[0];

            // users collection find user with id
            foreach (var user in UserData.users)
            {
                if (user.UserName == username)
                {
                    // get user and set status to active
                    user.Membership = true;
                    return "Membership renewal succesful";
                }
            }

            return "User not found.";
        }
    }
}

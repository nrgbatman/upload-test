﻿using Librari___TeamWork.Contracts;
using Librari___TeamWork.Core.Contracts;
using Librari___TeamWork.Core.Factories;
using Librari___TeamWork.Database;
using System;
using System.Collections.Generic;


namespace Librari___TeamWork.Core.Commands
{
    public class ChangePasswordCommand : ICommand
    {
        private readonly ILibraryFactory factory;
        private readonly IEngine engine;
        public ChangePasswordCommand(ILibraryFactory factory, IEngine engine)
        {
            this.factory = factory;
            this.engine = engine;
        }

        public string Execute(IList<string> parameters)
        {
            if (parameters.Count != 0 )
            {
                //Check for better msg
                throw new ArgumentException("ChangePassword command invalid parameters");
            }

            Messages.Messages.EnterPassword();
            string checkingPassword = Console.ReadLine();

            if (UserData.users[DataValidation.userListId].Password == checkingPassword)
            {
                Messages.Messages.EnterNewPassword();
                string newPassword = Console.ReadLine();

                //Add try catch block her!? if passwords missmatch
                Messages.Messages.ReEnterNewPassword();
                if (newPassword == Console.ReadLine())
                {
                    UserData.users[DataValidation.userListId].Password = newPassword;
                    return $"Password successfully changed.";
                }
            }

            return $"Invalid password";
        }
    }
}

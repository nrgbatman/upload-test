﻿using Librari___TeamWork.Contracts;
using Librari___TeamWork.Core.Contracts;
using Librari___TeamWork.Core.Factories;
using Librari___TeamWork.Database;
using System;
using System.Collections.Generic;
using System.Text;

namespace Librari___TeamWork.Core.Commands
{
    public class SearchAuthorCommand : ICommand
    {
        private readonly ILibraryFactory factory;
        private readonly IEngine engine;

        public SearchAuthorCommand(ILibraryFactory factory, IEngine engine)
        {
            this.factory = factory;
            this.engine = engine;
        }

        public string Execute(IList<string> parameters)
        {
            string author;

            if (parameters.Count != 1)
            {
                throw new ArgumentException("Please enter author name");
            }

            if (BooksData.bookList.Count == 0)
            {
                throw new ArgumentException("Library is empty");
            }

            try
            {
                author = parameters[0];

            }
            catch
            {
                throw new ArgumentException("Failed to parse Author command parameters.");
            }



            var str = new StringBuilder();
            foreach (var book in BooksData.bookList)
            {
                if (book.Author.ToLower() == author.ToLower())
                {
                    str.AppendLine(book.ToString());
                }
            }

            if (str.ToString().Length == 0)
            {
                throw new ArgumentException($"Books with author - {author} not found");
            }

            return str.ToString();
        }
    }
}

﻿using Librari___TeamWork.Contracts;
using Librari___TeamWork.Core.Contracts;
using Librari___TeamWork.Core.Factories;
using Librari___TeamWork.Database;
using Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Librari___TeamWork.Core.Commands
{
    public class CheckoutBookCommand : ICommand
    {
        private readonly ILibraryFactory factory;
        private readonly IEngine engine;
        public CheckoutBookCommand(ILibraryFactory factory, IEngine engine)
        {
            this.factory = factory;
            this.engine = engine;
        }

        public string Execute(IList<string> parameters)
        {
            int id;

            var currentUsername = UserData.users[DataValidation.userListId].UserName;
            List<Book> userBookList = UsersBooksLists.userBooksLists[currentUsername];

            //Checking if user already book 5 books 
            if (userBookList.Count >= 5)
            {
                throw new ArgumentException("Cannot checkout more than five books!");
            }
            try
            {
                id = int.Parse(parameters[0]);
            }
            catch (Exception)
            {
                throw new ArgumentException("Please enter Book ID");
            }


            //Check into userBooksList if we already have this book? if true = throw ex else continue
            if (UsersBooksLists.userBooksLists[UserData.users[DataValidation.userListId].UserName].Where(x => x.Id == id).ToList().Count > 0)
            {
                throw new ArgumentException("You already book this book! You cant book more copies!");
            }


            //AddReservation list and check is it reserved!?
            foreach (var book in BooksData.bookList)
            {
                if (book.Id == id)
                {
                    //Check qty, is it reserver ? Who reserver it ?!? date?! time
                    if (book.BookItem.BookItemCount > 0)
                    {
                        if (ReservedBooks.reservedBooks[id].IndexOf(currentUsername) < book.BookItem.BookItemCount)
                        {
                            var bookToAdd = book;
                            bookToAdd.BookItem.ItemCheckout = DateTime.Now;
                            userBookList.Add((Book)bookToAdd);

                            ReservedBooks.reservedBooks[id].Remove(currentUsername);
                        }

                        UsersBooksLists.userBooksLists[currentUsername] = userBookList;
                        book.BookItem.BookItemCount--;

                        StringBuilder str = new StringBuilder(250);
                        str.AppendLine($"{book.Title} added to {currentUsername} book list.");
                        str.AppendLine($"========== Your current book list ========== ");

                        foreach (var item in userBookList)
                        {
                            str.AppendLine($"Book Title: {item.Title} | " +
                                           $"Book ID: {item.Id} | " +
                                           $"Author {item.Author} | " +
                                           $"Checked-out at {item.BookItem.ItemCheckout.ToLongDateString()}");
                        }

                        str.AppendLine($"============================================= ");

                        return str.ToString();
                    }
                }
            }

            return $"Book not found";
        }
    }
}

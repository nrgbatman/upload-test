﻿using Librari___TeamWork.Contracts;
using Librari___TeamWork.Core.Contracts;
using Librari___TeamWork.Core.Factories;
using Librari___TeamWork.Database;
using System;
using System.Collections.Generic;
using System.Text;

namespace Librari___TeamWork.Core.Commands
{
    public class SearchIDCommand : ICommand
    {
        private readonly ILibraryFactory factory;
        private readonly IEngine engine;

        public SearchIDCommand(ILibraryFactory factory, IEngine engine)
        {
            this.factory = factory;
            this.engine = engine;
        }

        public string Execute(IList<string> parameters)
        {
            int id;

            if (parameters.Count != 1)
            {
                throw new ArgumentException("Please enter book ID");
            }

            if (BooksData.bookList.Count == 0)
            {
                throw new ArgumentException("Library is empty");
            }

            try
            {
                id = int.Parse(parameters[0]);

            }
            catch
            {
                throw new ArgumentException("Failed to parse Search ID command parameters.");
            }

            foreach (var book in BooksData.bookList)
            {
                if (book.Id == id)
                {
                    return book.ToString();
                }
            }

            return $"Book with {id} not found";
        }
    }
}

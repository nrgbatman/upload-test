﻿using Librari___TeamWork.Contracts;
using Librari___TeamWork.Core.Contracts;
using Librari___TeamWork.Core.Factories;
using Librari___TeamWork.Database;
using System;
using System.Collections.Generic;
using System.Text;

namespace Librari___TeamWork.Core.Commands
{
    public class SearchCatalogCommand : ICommand
    {
        private readonly ILibraryFactory factory;
        private readonly IEngine engine;

        public SearchCatalogCommand(ILibraryFactory factory, IEngine engine)
        {
            this.factory = factory;
            this.engine = engine;
        }

        public string Execute(IList<string> parameters)
        {
            if (BooksData.bookList.Count == 0)
            {
                throw new ArgumentException("The library is empty");
            }

            var str = new StringBuilder(500);
            foreach (var book in BooksData.bookList)
            {
                str.AppendLine(book.ToString());
            }

            return str.ToString();
        }
    }
}

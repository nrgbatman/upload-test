﻿using Librari___TeamWork.Contracts;
using Librari___TeamWork.Core.Contracts;
using Librari___TeamWork.Core.Factories;
using Librari___TeamWork.Database;
using System;
using System.Collections.Generic;
using System.Text;

namespace Librari___TeamWork.Core.Commands
{
    public class SearchISBNCommand : ICommand
    {
        private readonly ILibraryFactory factory;
        private readonly IEngine engine;

        public SearchISBNCommand(ILibraryFactory factory, IEngine engine)
        {
            this.factory = factory;
            this.engine = engine;
        }

        public string Execute(IList<string> parameters)
        {
            string ISBN;

            if (parameters.Count != 1)
            {
                throw new ArgumentException("Please enter ISBN of book");
            }

            if (BooksData.bookList.Count == 0)
            {
                throw new ArgumentException("Library is empty");
            }

            try
            {
                ISBN = parameters[0];

            }
            catch
            {
                throw new ArgumentException("Failed to parse SearchISBN command parameters.");
            }

            var str = new StringBuilder();

            foreach (var book in BooksData.bookList)
            {
                if (book.Isbn.ToLower() == ISBN.ToLower())
                {
                    str.AppendLine(book.ToString());
                }
            }

            if (str.ToString().Length == 0)
            {
                throw new ArgumentException($"Books with ISBN - {ISBN} not found");
            }

            return str.ToString();
        }
    }
}

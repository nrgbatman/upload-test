﻿using Librari___TeamWork.Contracts;
using Librari___TeamWork.Core.Contracts;
using Librari___TeamWork.Core.Factories;
using Librari___TeamWork.Database;
using System;
using System.Collections.Generic;
using System.Text;

namespace Librari___TeamWork.Core.Commands
{
    public class SearchYearCommand : ICommand
    {
        private readonly ILibraryFactory factory;
        private readonly IEngine engine;

        public SearchYearCommand(ILibraryFactory factory, IEngine engine)
        {
            this.factory = factory;
            this.engine = engine;
        }

        public string Execute(IList<string> parameters)
        {
            int year;

            if (parameters.Count != 1)
            {
                throw new ArgumentException("Please enter year");
            }

            if (BooksData.bookList.Count == 0)
            {
                throw new ArgumentException("Library is empty");
            }

            try
            {
                year = int.Parse(parameters[0]);

            }
            catch
            {
                throw new ArgumentException("Failed to parse title command parameters. Please enter correct Y");
            }
            //Checking year value <= now
            if (year > DateTime.Parse(DateTime.Now.ToString()).Year)
            {
                throw new ArgumentException($"Invalid year, please enter year before {DateTime.Now.Year}.");
            }

            var str = new StringBuilder();
            foreach (var book in BooksData.bookList)
            {
                if (book.Year == year)
                {
                    str.AppendLine(book.ToString());
                }
            }

            if (str.ToString().Length == 0)
            {
                throw new ArgumentException($"Books with year {year} not found.");
            }

            return str.ToString();
        }
    }
}

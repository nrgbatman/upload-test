﻿using Librari___TeamWork.Contracts;
using Librari___TeamWork.Core.Contracts;
using Librari___TeamWork.Core.Factories;
using Librari___TeamWork.Database;
using System;
using System.Collections.Generic;
using System.Text;

namespace Librari___TeamWork.Core.Commands
{
    public class SearchTitleCommand : ICommand
    {
        private readonly ILibraryFactory factory;
        private readonly IEngine engine;

        public SearchTitleCommand(ILibraryFactory factory, IEngine engine)
        {
            this.factory = factory;
            this.engine = engine;
        }

        public string Execute(IList<string> parameters)
        {
            string title;

            if (parameters.Count != 1)
            {
                throw new ArgumentException("Please enter title name");
            }

            if (BooksData.bookList.Count == 0)
            {
                throw new ArgumentException("Library is empty");
            }

            try
            {
                title = parameters[0];

            }
            catch
            {
                throw new ArgumentException("Failed to parse SearchTitle command parameters.");
            }

            var str = new StringBuilder();

            foreach (var book in BooksData.bookList)
            {
                if (book.Title.ToLower() == title.ToLower())
                {
                    str.AppendLine(book.ToString());
                }
            }

            if (str.ToString().Length == 0)
            {
                throw new ArgumentException($"Books with title - {title} not found");
            }

            return str.ToString();
        }
    }
}

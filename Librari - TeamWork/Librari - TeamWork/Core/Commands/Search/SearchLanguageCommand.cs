﻿using Librari___TeamWork.Contracts;
using Librari___TeamWork.Core.Contracts;
using Librari___TeamWork.Core.Factories;
using Librari___TeamWork.Database;
using System;
using System.Collections.Generic;
using System.Text;

namespace Librari___TeamWork.Core.Commands
{
    public class SearchLanguageCommand : ICommand
    {
        private readonly ILibraryFactory factory;
        private readonly IEngine engine;

        public SearchLanguageCommand(ILibraryFactory factory, IEngine engine)
        {
            this.factory = factory;
            this.engine = engine;
        }

        public string Execute(IList<string> parameters)
        {
            string language;

            if (parameters.Count != 1)
            {
                throw new ArgumentException("Please enter language");
            }
            try
            {
                language = parameters[0];
            }
            catch (Exception)
            {

                throw new ArgumentException("Invalid SearchLanguage command parameters");
            }

            var str = new StringBuilder(200);
            foreach (var book in BooksData.bookList)
            {
                if (book.Language.ToLower() == language.ToLower())
                {
                    str.AppendLine(book.ToString());
                }

            }

            if (str.ToString().Length == 0)
            {
                throw new ArgumentException($"Books with language - {language} not found");
            }

            return str.ToString();
        }
    }
}

﻿using Librari___TeamWork.Contracts;
using Librari___TeamWork.Core.Contracts;
using Librari___TeamWork.Core.Factories;
using Librari___TeamWork.Database;
using System;
using System.Collections.Generic;

namespace Librari___TeamWork.Core.Commands
{
    public class RemoveBookCommand : ICommand
    {
        private readonly ILibraryFactory factory;
        private readonly IEngine engine;

        public RemoveBookCommand(ILibraryFactory factory, IEngine engine)
        {
            this.factory = factory;
            this.engine = engine;
        }
        public string Execute(IList<string> parameters)
        {
            int id;

            try
            {
                id = int.Parse(parameters[0]);
            }
            catch (Exception)
            {

                throw new ArgumentException("Invalid Remove Book Command Parameters");
            }

            int counter = -1;
            bool isFound = false;

            foreach (var item in BooksData.bookList)
            {
                counter++;
                if (item.Id == id)
                {
                    isFound = true;
                    break;
                }
            }
            if (isFound == false)
            {
                throw new ArgumentException("Book ID not found");
            }

            this.factory.RemoveBook(counter);

            return $"Book with ID {id} removed";
 
        }
    }
}

﻿using Librari___TeamWork.Contracts;
using Librari___TeamWork.Core.Contracts;
using Librari___TeamWork.Core.Factories;
using Librari___TeamWork.Database;
using Library;
using System;
using System.Collections.Generic;
using System.Text;

namespace Librari___TeamWork.Core.Commands
{
    public class ListMyBooksCommand : ICommand
    {

        private readonly ILibraryFactory factory;
        private readonly IEngine engine;
        public ListMyBooksCommand(ILibraryFactory factory, IEngine engine)
        {
            this.factory = factory;
            this.engine = engine;
        }
        public string Execute(IList<string> parameters)
        {

            if (parameters.Count != 0)
            {
                //Think about better msg!?
                throw new ArgumentException("ListMyBooks command require 0 params!"); 
            }


            //We access current user bookList 
            List<Book> userBookList = UsersBooksLists.userBooksLists[UserData.users[DataValidation.userListId].UserName];

            StringBuilder str = new StringBuilder(500);
            foreach (var book in userBookList)
            {
                str.AppendLine($"Book title: {book.Author} | " +
                                         $"Book ID: {book.Id} | " +
                                         $"Author: {book.Author} | " +
                                         $"CheckOut at: {DateTime.Now.ToLongDateString()}");



            }

            if (str.ToString().Length == 0)
            {
                Console.WriteLine($"Book list of current user is empty");
            }

            return str.ToString();

        }
    }
}

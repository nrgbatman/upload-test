﻿using Librari___TeamWork.Contracts;
using Librari___TeamWork.Core.Contracts;
using Librari___TeamWork.Core.Factories;
using Librari___TeamWork.Database;
using Library;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Librari___TeamWork.Core.Commands
{
    public class RenewBookCommand : ICommand
    {
        private readonly ILibraryFactory factory;
        private readonly IEngine engine;

        public RenewBookCommand(ILibraryFactory factory, IEngine engine)
        {
            this.factory = factory;
            this.engine = engine;
        }
        public string Execute(IList<string> parameters)
        {
            //Only members can renewbooks.
            if (DataValidation.IsLibrarianStatus())
            {
                throw new ArgumentException(Messages.Messages.InvalidCommand());
            }

            if (parameters.Count != 1)
            {
                throw new ArgumentException("Invalid Renew parameters. Please enter the ID of book");
            }

            int id;

            try
            {
                id = int.Parse(parameters[0]);
            }
            catch (Exception)
            {
                throw new ArgumentException("Cannot parse Renew command parameters!");
            }

            List<Book> userBookList = UsersBooksLists.userBooksLists[UserData.users[DataValidation.userListId].UserName];

            if (userBookList.Count == 0)
            {
                throw new ArgumentException("Your book list is empty.");
            }

            if(userBookList.Where(x => x.Id == id).ToList().Count > 0)
            {
                userBookList = userBookList.Where(x => x.Id != id).ToList();

                foreach (var item in userBookList)
                {
                    //item.BookItem.ItemCheckout 


                   

                }
            }

            return "";


        }
    }
}

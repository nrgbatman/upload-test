﻿using Librari___TeamWork.Contracts;
using Librari___TeamWork.Core.Contracts;
using Librari___TeamWork.Core.Factories;
using Librari___TeamWork.Database;
using System;
using System.Collections.Generic;

namespace Librari___TeamWork.Core.Commands
{
    public class EditBookCommand : ICommand
    {
        private readonly ILibraryFactory factory;
        private readonly IEngine engine;


        public EditBookCommand(ILibraryFactory factory, IEngine engine)
        {
            this.factory = factory;
            this.engine = engine;
        }
        public string Execute(IList<string> parameters)
        {
            //Checking type of logged user
            if (!DataValidation.IsLibrarianStatus())
            {
                throw new ArgumentException(Messages.Messages.InvalidCommand());
            }

            int id;
            int bookItemCount;

            try
            {
                id = int.Parse(parameters[0]);
                bookItemCount = int.Parse(parameters[1]);

            }
            catch (Exception)
            {

                throw new ArgumentException("Invalid Edit Book Command Parameters");
            }

            int counter = -1;
            bool isFound = false;

            foreach (var item in BooksData.bookList)
            {
                counter++;
                if (item.Id == id)
                {
                    isFound = true;
                    break;
                }
            }

            if (isFound == false)
            {
                throw new ArgumentException("Book ID not found");
            }
            foreach (var item in BooksData.bookList)
            {
                if (item.BookItem.BookItemCount != bookItemCount)
                {
                    item.BookItem.BookItemCount = bookItemCount;
                }
            }

            return $"Book with ID {id} edited";
        }
    }
}

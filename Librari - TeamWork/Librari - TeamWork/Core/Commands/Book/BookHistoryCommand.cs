﻿using Librari___TeamWork.Contracts;
using Librari___TeamWork.Core.Contracts;
using Librari___TeamWork.Core.Factories;
using Librari___TeamWork.Database;
using System;
using System.Collections.Generic;
using System.Text;

namespace Librari___TeamWork.Core.Commands
{
    public class BookHistoryCommand : ICommand
    {

        private readonly ILibraryFactory factory;
        private readonly IEngine engine;
        public BookHistoryCommand(ILibraryFactory factory, IEngine engine)
        {
            this.factory = factory;
            this.engine = engine;
        }
        public string Execute(IList<string> parameters)
        {
            //Check status of current user
            if (!DataValidation.IsLibrarianStatus())
            {
                throw new ArgumentException(Messages.Messages.InvalidCommand());
            }

            if (parameters.Count != 1)
            {
                throw new ArgumentException("Please enter book ISBN");
            }

            string ISBN;

            try
            {
                ISBN = parameters[0];
            }
            catch (Exception)
            {

                throw new ArgumentException("Invalid BookInfoCommand parameters! Please enter ISBN");
            }

            StringBuilder str = new StringBuilder();

            foreach (var item in BooksData.bookList)
            {
                if (item.Isbn == ISBN)
                {
                    str.AppendLine();
                    str.AppendLine($"======= Overall history of Book with ISBN - {ISBN} =======");
                    foreach (var lineOfData in item.History)
                    {
                        str.AppendLine(lineOfData);
                    }

                    return str.ToString();
                }
            }

            return $"No history found for book with ISBN - {ISBN}";
        }
    }
}

﻿using Librari___TeamWork.Contracts;
using Librari___TeamWork.Core.Contracts;
using Librari___TeamWork.Core.Factories;
using Librari___TeamWork.Database;
using Library;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Librari___TeamWork.Core.Commands
{
    public class ReturnBookCommand : ICommand
    {
        private readonly ILibraryFactory factory;
        private readonly IEngine engine;
        public ReturnBookCommand(ILibraryFactory factory, IEngine engine)
        {
            this.factory = factory;
            this.engine = engine;
        }
        public string Execute(IList<string> parameters)
        {
            //Check status of current user // Only members can use this command.
            if (DataValidation.IsLibrarianStatus())
            {
                throw new ArgumentException(Messages.Messages.InvalidCommand());
            }

            int id;

            try
            {
                id = int.Parse(parameters[0]);
            }
            catch (Exception)
            {

                throw new ArgumentException("Please enter Book ID");
            }

            //We can delete this row 
            var list = UsersBooksLists.userBooksLists[UserData.users[DataValidation.userListId].UserName];

            foreach (Book item in list)
            {
                if (item.Id == id)
                {
                    //We add 1 item to book qty
                    BooksData.bookList.Single(x => x.Id == id).BookItem.BookItemCount++;

                    //We add history to books by ID
                    BooksData.bookList.Single
                        (x => x.Id == id).History.Add
                        ($"{UserData.users[DataValidation.userListId].UserName} " +
                         $"return book at { DateTime.Now.ToLongDateString()}");
              

                    //We remove book from userBookList;
                    UsersBooksLists.userBooksLists[UserData.users[DataValidation.userListId].UserName] =
                    UsersBooksLists.userBooksLists[UserData.users[DataValidation.userListId].UserName]
                    .Where(x => x.Id != id).ToList();
                    return $"Book with ID - {id} and ISBN - {item.Isbn} successfully returned";
                }
            }

            return $"Book with ID - {id} not found";
        }
    }
}

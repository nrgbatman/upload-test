﻿using Librari___TeamWork.Contracts;
using Librari___TeamWork.Core.Contracts;
using Librari___TeamWork.Core.Factories;
using Librari___TeamWork.Database;
using Librari___TeamWork.Users;
using Librari___TeamWork.Users.Contracts;
using Library;
using System;
using System.Collections.Generic;
using System.Text;

namespace Librari___TeamWork.Core.Commands
{
    public class CancelMembershipCommand : ICommand
    {
        private readonly ILibraryFactory factory;
        private readonly IEngine engine;
        public CancelMembershipCommand(ILibraryFactory factory, IEngine engine)
        {
            this.factory = factory;
            this.engine = engine;
        }
        public string Execute(IList<string> parameters)
        {

            if (parameters.Count != 0)
            {
                throw new ArgumentException("Cancel membership command don't require any parameters");
            }

            Messages.Messages.EnterPassword();
            string checkingPassword = Console.ReadLine();

            if (UserData.users[DataValidation.userListId].Password == checkingPassword)
            {
                UserData.users[DataValidation.userListId].Membership = false;
                return $"Membership cancelled successfully";
            }

            return "Invalid password";




        }
    }
}

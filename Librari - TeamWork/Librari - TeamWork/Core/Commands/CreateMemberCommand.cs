﻿using Librari___TeamWork.Contracts;
using Librari___TeamWork.Core.Contracts;
using Librari___TeamWork.Core.Factories;
using Librari___TeamWork.Database;
using Librari___TeamWork.Core.Factories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Librari___TeamWork.Core.Commands
{
    public class CreateMemberCommand : ICommand
    {
        private readonly ILibraryFactory factory;
        private readonly IEngine engine;

        public CreateMemberCommand(ILibraryFactory factory, IEngine engine)
        {
            this.factory = factory;
            this.engine = engine;
        }

        public string Execute(IList<string> parameters)
        {
            string firstName;
            string lastName;
            string userName;
            string password;
            bool membership;

            if (parameters.Count != 5)
            {
                throw new ArgumentException("Invalid CreateMember parameters! " +
                                            "Please enter the following paramters: " +
                                            "firstname,lastname,username,password and membership!");
            }

            try
            {
                firstName = parameters[0];
                lastName = parameters[1];
                userName = parameters[2];
                password = parameters[3];
                membership = bool.Parse(parameters[4]);

            }
            catch (Exception)
            {

                throw new ArgumentException("Cannot parse CreateMember parameters!");
            }


            var user = this.factory.CreateMember(firstName, lastName, userName, password, membership);
            UserData.users.Add(user);

            return $"Member {firstName} {lastName} with username - {userName} created!";

        }
    }
}

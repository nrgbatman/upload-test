﻿using System;
using System.Collections.Generic;
using Library.Models;
using Librari___TeamWork.Contracts;
using Librari___TeamWork.Core.Contracts;
using Librari___TeamWork.Core.Factories;
using Librari___TeamWork.Database;

namespace Librari___TeamWork.Core.Commands.Creating
{
    public class CreateBookCommand : ICommand
    {
        private readonly ILibraryFactory factory;
        private readonly IEngine engine;

        public CreateBookCommand(ILibraryFactory factory, IEngine engine)
        {
            this.factory = factory;
            this.engine = engine;
        }

        public string Execute(IList<string> parameters)
        {
            string title;
            string author;
            string category;
            string isbn;
            string language;
            int year;
            int pages;
            BookItem bookItem;

            try
            {

                title = parameters[0];
                author = parameters[1];
                category = parameters[2];
                isbn = parameters[3];
                language = parameters[4];
                year = int.Parse(parameters[5]);
                pages = int.Parse(parameters[6]);
                bookItem = new BookItem(int.Parse(parameters[7]),
                                        int.Parse(parameters[8]));
                //DateTime.Parse(parameters[9]),
                //DateTime.Parse(parameters[10]), 
                //parameters[11]);

            }
            catch
            {
                throw new ArgumentException($"Please enter Title/Author/Category/ISBN/Language/Year/Pages/Books qty/Rack Number");
            }

            var book = this.factory.CreateBook(title, author, category, isbn, language, year, pages, bookItem);
            BooksData.addToBookList(book);

            return book.ToString();
        }
    }
}



﻿using Librari___TeamWork.Contracts;
using Librari___TeamWork.Core.Contracts;
using Librari___TeamWork.Core.Factories;
using Librari___TeamWork.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Librari___TeamWork.Core.Commands
{
    public class UserInfoCommand : ICommand
    {
        private readonly ILibraryFactory factory;
        private readonly IEngine engine;

        public UserInfoCommand(ILibraryFactory factory, IEngine engine)
        {
            this.factory = factory;
            this.engine = engine;
        }

        public string Execute(IList<string> parameters)
        {
            if (parameters.Count != 1)
            {
                //Think about better msg
                throw new ArgumentException("Please enter username.");
            }

            string userName;

            try
            {
                userName = parameters[0];
            }
            catch (Exception)
            {
                throw new ArgumentException("Invalid UserInfo command parameters!");
            }

            //We are checking for userName into UsersList;
            if (UserData.users.Where(x => x.UserName.ToLower() == userName.ToLower()).ToList().Count > 0)
            {
                StringBuilder str = new StringBuilder();

                //ADD blabla INFO firstName/lastName Membership etc
                foreach (var item in UsersBooksLists.userBooksLists[userName])
                {
                    str.AppendLine($"Book - {item.Title} " +
                                   $"Author - {item.Author} " +
                                   $"Year - {item.Year} " +
                                   $"CheckOutAt - {item.BookItem.ItemCheckout.ToLongDateString()}");
                    return str.ToString();
                }

            }

            //If username not found=>
            return $"User not found!";
        }
    }
}

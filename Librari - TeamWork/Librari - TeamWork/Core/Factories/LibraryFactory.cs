﻿using Librari___TeamWork.Users.Contracts;
using Library;
using Library.Contracts;
using Library.Models;
using Librari___TeamWork.Users;
using System;
using Librari___TeamWork.Database;

namespace Librari___TeamWork.Core.Factories
{
    public class LibraryFactory : ILibraryFactory
    {
        private static ILibraryFactory instanceHolder = new LibraryFactory();

        private LibraryFactory()
        {
        }
        public static ILibraryFactory Instance
        {
            get
            {
                return instanceHolder;
            }
        }
        public IPerson CreateMember(string firstName, string lastName, string username, string password, bool membership)
        {
            return new Member(firstName, lastName, username, password, membership);

        }
        public IBook CreateBook(string title, string author, string category, string isbn, string language, int year, int pages, BookItem bookItem)
        {
            SubjectCategory categoryParse = (SubjectCategory)Enum.Parse(typeof(SubjectCategory), category, true);

            return new Book(title, author, categoryParse, isbn, language, year, pages, bookItem);
        }
        public void RemoveBook(int counter)
        {
            BooksData.bookList.RemoveAt(counter);
        }

    }
}

﻿using Librari___TeamWork.Users.Contracts;
using Library.Contracts;
using Library.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Librari___TeamWork.Core.Factories
{
    public interface ILibraryFactory
    {
        IBook CreateBook(string title, string author, string category, string isbn, string language, int year, int pages, BookItem bookItem);

        IPerson CreateMember(string firstName, string lastName, string username, string password, bool membership);

        void RemoveBook(int counter);
    }
}

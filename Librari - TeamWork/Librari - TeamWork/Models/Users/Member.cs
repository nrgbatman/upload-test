﻿using Librari___TeamWork.Users.Contracts;
using Library;
using System;
using Librari___TeamWork.Database;
using System.Collections.Generic;
using System.Text;

namespace Librari___TeamWork.Users
{
    public class Member : IMember
    {

        private string firstName;
        private string lastName;
        private string userName;
        private string password;
        private DateTime lastLogIn;
        private bool membership;
        private readonly bool admin = false;
        private readonly DateTime registeredAt;

        public Member(string firstName, string lastName, string username, string password, bool membership)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.UserName = username;
            this.Password = password;
            UsersBooksLists.userBooksLists.Add(username, new List<Book>());
            this.Membership = membership;
            LastLogIn = DateTime.Now;
            registeredAt = DateTime.Now;
        }

        public string FirstName 
        {
            get
            {
                return this.firstName;
            }
            private set
            {
                if (value.Length < 3 || value.Length > 20)
                {
                    throw new ArgumentException("First name must be between 3 and 20 characters");
                }
                firstName = value;
            }
        }
        public string LastName
        {
            get
            {
                return this.lastName;
            }
            private set
            {
                if (value.Length < 3 || value.Length > 20)
                {
                    throw new ArgumentException("First name must be between 3 and 20 characters");
                }
               
                this.lastName = value;
            }
        }
        public string UserName
        {
            get
            {
                return this.userName;
            }
            private set
            {
                foreach (var member in UserData.users)
                {
                    if (member.UserName.ToLower() == value)
                    {
                        throw new ArgumentException("The user already exists. Please enter different username!");
                    }

                }

                if (value.Length < 5 || value.Length > 15)
                {
                    throw new ArgumentException("Username must be between 5 and 15 characters");
                }
                
                this.userName = value.ToLower();
            }
        }
        public string Password
        {
            get
            {
                return this.password;
            }
             set
            {

                //if (value.Length < 8)
                //{
                //    throw new ArgumentException("Password must be at least 8 characters long");
                //}
                //if (value == this.FirstName || value == this.LastName)
                //{
                //    throw new ArgumentException("Your password is easy to guess!");
                //}

                ////Checking does digit cointains in passowrd;
                //bool containsDigit = value.Where(x => Char.IsDigit(x)).Any();

                //if (containsDigit == false)
                //{
                //    throw new ArgumentException("Password must contain at least one number");
                //}
                this.password = value;

            }

        }
        public DateTime LastLogIn
        {
            get
            {
                return this.lastLogIn;
            }
            private set
            {
                this.lastLogIn = value;
            }
        }
        public bool Membership
        {
            get
            {
                return this.membership;
            }
            set
            {
                this.membership = value;
            }
        }
        public bool Admin
        {
            get
            {
                return this.admin;
            }
        }
        public DateTime RegisteredAt
        {
            get
            {
                return this.registeredAt;
            }
        }
        public override string ToString()
        {
            StringBuilder str = new StringBuilder();

            foreach (var item in UsersBooksLists.userBooksLists[userName])
            {
                str.AppendLine($"Book - {item.Title} " +
                               $"Author - {item.Author} " +
                               $"Year - {item.Year} " +
                               $"CheckOutAt - {item.BookItem.ItemCheckout.ToLongDateString()}");
            }

            return $"=============================== Access granted ========================================" + "\n" +
                   $"Welcome, {this.firstName} {this.LastName}" + "\n" +
                   $"You have logged in as Member at " + DateTime.Now.ToLongTimeString() + "\n" + //Think about better msg
                   $"Last login at {LastLogIn.ToLongDateString()}" + "\n" +
                   $"============================== List of checkout books ================================" + "\n" +
                   str.ToString();
        }
    }
}

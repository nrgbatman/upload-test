﻿using Librari___TeamWork.Database;
using System;


namespace Librari___TeamWork.Users.Contracts
{
    public class Librarian : ILibrarian
    {

        private string firstName;
        private string lastName;
        private string userName;
        private string password;
        private DateTime lastLogIn;
        private readonly bool membership;
        private readonly bool admin = true;
        public Librarian(string firstName, string lastName, string userName, string password)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.UserName = userName;
            this.password = password;
            LastLogIn = DateTime.Now;
            this.membership = true;
        }

        public string FirstName
        {
            get { return this.firstName; }
            private set
            {
                if (value.Length < 3 || value.Length > 12)
                {
                    throw new ArgumentException("Please enter name between 3 and 12 characters");
                }

                firstName = value;
            }
        }
        public string LastName
        {
            get
            {
                return this.lastName;
            }
            private set
            {
                if (value.Length < 3 || value.Length > 12)
                {
                    throw new ArgumentException("Please enter last name between 3 and 12 characters");
                }

                lastName = value;
            }
        }
        public string UserName
        {
            get
            {
                return this.userName;
            }
            private set
            {
                //Can we use LINQ?
                foreach (var user in UserData.users)
                {
                    if (user.UserName.ToLower() == value.ToLower())
                    {
                        throw new ArgumentException("Username is already exist");
                        
                    }

                }
                this.userName = value.ToLower();
            }
        }
        public string Password
        {
            get
            {
                return this.password;
            }
            set
            {
                this.password = value;
                //if (value.Length < 8)
                //{
                //    throw new ArgumentException("Password must be at least 8 characters long");
                //}
                //if (value == this.FirstName || value == this.LastName)
                //{
                //    throw new ArgumentException("Your password is easy to guess!");
                //}

                ////Checking does digit cointains in passowrd;
                //bool containsDigit = value.Where(x => Char.IsDigit(x)).Any();   

                //if (containsDigit == false)
                //{
                //    throw new ArgumentException("Password must contain at least one number");
                //}
            }
        }
        public DateTime LastLogIn
        {
            get
            {
                return this.lastLogIn;
            }
            set
            {
                this.lastLogIn = value;
            }
        }
        public bool Membership
        {
            get
            {
                return this.membership;
            }
            set
            {
            }
        }
        public bool Admin
        {
            get
            {
                return this.admin;
            }
        }
        public override string ToString()
        {
            return $"=============================== Access granted =========================================" + "\n" +
                   $"Welcome {this.firstName} {this.LastName}" + "\n" +
                   $"You have logged as Librarian " + "\n" +
                   $"Last logged on : {this.LastLogIn.ToLongTimeString()}" + "\n" +
                   $"========================================================================================" + "\n" +
                   $"Enter -help for list of commands "; //TODO to be implemented
        }
    }
}

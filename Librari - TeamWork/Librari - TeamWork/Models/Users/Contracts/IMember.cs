﻿

namespace Librari___TeamWork.Users.Contracts
{
    interface IMember : IPerson 
    {
        //We use keyword new to hide inheritance
        new bool Membership { get; set; } 
    }
}

﻿

using System;

namespace Librari___TeamWork.Users.Contracts
{
    
    public interface IPerson
    {

        string FirstName { get; }
        string LastName { get; }
        string UserName { get; }
        string Password { get;  set; }
        bool Membership { get; set; }
        bool Admin { get; }

 
    }
}

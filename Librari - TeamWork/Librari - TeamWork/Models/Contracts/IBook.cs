﻿using Library.Models;
using System;
using System.Collections.Generic;

namespace Library.Contracts
{
    public interface IBook
    {
        int Id { get; }
        int Year { get; }
        int Pages { get; }
        string Title { get; }
        string Author { get; }
        string Isbn { get; }
        string Language { get; }
        string ToString();
        SubjectCategory Category { get; set; }
        List<string> History { get; set; }
        BookItem BookItem { get; }
    }
}

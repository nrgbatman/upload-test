﻿namespace Library.Models
{
    public enum SubjectCategory
    {
        Art = 0,
        Biography = 1,
        ChickLit = 2,
        Children = 3,
        Classics = 4,
        Comics = 5,
        Contemporary = 6,
        Crime = 7,
        Fantasy = 8,
        Mystery = 9,
        Romance = 10,
        SciFi = 11,
        Suspense = 12,
        Thriller = 13,
        YoungAdult = 14
    }
}

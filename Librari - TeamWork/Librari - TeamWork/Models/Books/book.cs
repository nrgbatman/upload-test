﻿using Librari___TeamWork.Database;
using Library.Contracts;
using Library.Models;
using System;
using System.Collections.Generic;

namespace Library
{
    public class Book : IBook
    {
        
        private static int bookId = 1;
        private int id;
        private string title;
        private string author;
        private string isbn;
        private string language;
        private int year;
        private int pages;
        private BookItem bookItem;
        public List<string> history = new List<string>();

        public Book(string title, string author, SubjectCategory category, string isbn, string language, int year, int pages, BookItem bookItem)
        {
            this.Id = bookId;
            bookId++;
            this.Title = title;
            this.Author = author;
            this.Isbn = isbn;
            this.Language = language;
            this.Year = year;
            this.Pages = pages;
            this.BookItem = bookItem;
            ReservedBooks.reservedBooks.Add(Id, new List<string>());
        }

        public int Id
        {
            get
            {
                return this.id;
            }
            private set
            {
                this.id = value;
            }
        }
        public string Title
        {
            get
            {
                return this.title;
            }
            private set
            {

                this.title = value;

            }
        }
        public string Author
        {
            get
            {
                return this.author;
            }
            private set
            {

                this.author = value;
            }
        }
        public string Isbn
        {
            get
            {
                return this.isbn;
            }
            private set
            {

                foreach (var item in BooksData.bookList)
                {
                    if (item.Isbn == value)
                    {
                        throw new ArgumentException("The book already exists");
                    }
                }

                this.isbn = value;
            }
        }
        public string Language
        {
            get
            {
                return this.language;
            }
            private set
            {
                this.language = value;
            }
        }
        public int Year
        {
            get
            {
                return this.year;
            }
            private set
            {
                if (value > DateTime.Now.Year) //TODO Use DateTime.Year!? Check for it
                {
                    throw new ArgumentException("Invalid year parameters!"); //TODO fix this msg
                }
                this.year = value;
            }
        }
        public int Pages
        {
            get
            {
                return this.pages;
            }
            private set
            {
                this.pages = value;
            }
        }
        public BookItem BookItem
        {
            get
            {
                return this.bookItem;
            }
            private set
            {
                this.bookItem = value;
            }
        }
        public SubjectCategory Category { get; set; }
        public List<string> History
        {
            get
            {
                return this.history;
            }
            set
            {
                this.history.Add(value.ToString());
            }
        }
        public override string ToString()
        {
            return $"============== ID: { this.Id} ===============" + "\n" +
                   $"Book with title: {this.Title}" + "\n" +
                   $"Author: {this.Author}" + "\n" +
                   $"Category: {this.Category}" + "\n" +
                   $"Language: {this.Language}" + "\n" +
                   $"The year of the book is {this.Year}" + "\n" +
                   $"With {this.Pages} pages." + "\n" +
                   $"ISBN: {Isbn}." + "\n"+
                   $"Aveable copies: {BookItem.BookItemCount}" + "\n" +
                   $"Rack number: {BookItem.RackNumber}" + "\n" +
                   $"====================================";
        }
    }
}

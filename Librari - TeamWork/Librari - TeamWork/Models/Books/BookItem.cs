﻿using System;
using System.Collections.Generic;

public class BookItem
{
    private int bookItemCount;
    private int rackNumber;
    private DateTime itemCheckout;
    private DateTime returnDate;
    private DateTime itemAdded;
    
    public BookItem(int bookItemCount, int rackNumber)
    {
        this.BookItemCount = bookItemCount;
        this.RackNumber = rackNumber;
        itemAdded = DateTime.Now;
    }

    public int BookItemCount
    {
        get
        {
            return this.bookItemCount;
        }
         set
        {
            this.bookItemCount = value;
        }
    }
    public int RackNumber
    {
        get
        {
            return this.rackNumber;
        }
        private set
        {
            this.rackNumber = value;
        }
    }
    public DateTime ItemCheckout
    {
        get
        {
            return this.itemCheckout;
        }
         set
        {
            this.itemCheckout = value;
        }
    }
    public DateTime ItemAdded
    {
        get
        {
            return this.itemAdded;
        }
        private set
        {
            this.itemAdded = value;
        }
    }
    public DateTime ReturnDate
    {
        get
        {
            return this.returnDate;
        }
         set
        {
            this.returnDate = value;
        }
    }

}

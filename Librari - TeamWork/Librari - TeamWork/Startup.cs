﻿using Librari___TeamWork.Core.Contracts;
using Librari___TeamWork.Core.Providers;
using Librari___TeamWork.Database;
using System;

namespace Librari___TeamWork
{
    public class Program
    {
        public static void Main()
        {
            //TODO change this to instance or smth else
            var engine = new Engine();
            engine.LoadData();
            engine.Start();
        }
    }
}

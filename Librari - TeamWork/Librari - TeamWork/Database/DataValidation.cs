﻿using System;


namespace Librari___TeamWork.Database
{
    public static class DataValidation
    {
        public static int userListId; // Can we change it to readonly !? for more privacy

        public static Tuple<bool, int> UserValidation(string user, string passwrod)
        {
            for (int i = 0; i < UserData.users.Count; i++)
            {
                if (UserData.users[i].UserName == user && UserData.users[i].Password == passwrod)
                {
                    userListId = i;
                    return new Tuple<bool, int>(true, i);
                }
            }
            
            return new Tuple<bool, int>(false, 0);

        }
        public static bool IsLibrarianStatus()
        {
            if(UserData.users[userListId].Admin)
            {
                return true;
            }

            return false;
        }

    }
}

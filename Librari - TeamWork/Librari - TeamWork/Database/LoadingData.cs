﻿using Librari___TeamWork.Users;
using Librari___TeamWork.Users.Contracts;
using Library;
using Library.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Librari___TeamWork.Database
{
    class LoadingData
    {
        public void LoadingUserData()
        {
            UserData.users.Add(new Librarian("Mihail", "Katsarov", "nrgbatman", "12345"));
            UserData.users.Add(new Librarian("Desislava", "Geleva", "desiuser", "desipassword"));
            UserData.users.Add(new Member("Gosho", "Jekov", "batman", "hello", true));
        }

        public void LoadingBooksData()
        {

            BooksData.bookList.Add(new Book("Harry Potter", "Author", SubjectCategory.Art, "asd", "EN", 2005, 100, new BookItem(1, 5)));
            BooksData.bookList.Add(new Book("Title1", "Author2", SubjectCategory.Art, "isbn2", "EN", 2001, 25, new BookItem(50, 2)));
            BooksData.bookList.Add(new Book("Title2", "Author3", SubjectCategory.Art, "asd1", "BG", 1999, 500, new BookItem(25, 1)));
            BooksData.bookList.Add(new Book("Title3", "Author4", SubjectCategory.Art, "asd2", "EN", 2004, 55, new BookItem(7, 3)));
        }

    }
}
